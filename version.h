// Produced by makever.pl.  Don't edit.
#define VER_MAJOR 0
#define VER_MINOR 3
#define VER_BUILD 7
#define VER_BUILD_TIME 1330136537
#ifndef STRINGIFY
#define ___S(X) #X
#define STRINGIFY(X) ___S(X)
#endif
#define VER_BUILD_TIME_STRING STRINGIFY(Fri Feb 24 21:22:17 2012)
#define VER_STRING STRINGIFY(VER_MAJOR) "." STRINGIFY(VER_MINOR) " (build " STRINGIFY(VER_BUILD) ", " VER_BUILD_TIME_STRING ")"
